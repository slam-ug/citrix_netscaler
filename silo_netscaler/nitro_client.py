from silo_netscaler.nitro_exceptions import NitroRequestException
from silo_netscaler.nitro_resources import NITRO_AUTH_TOKEN_KEY
import requests
from requests import ConnectTimeout, ConnectionError

class NitroClient(object):
    """
    Nitro client used to talk to Citrix Netscaler NITRO API
    """
    def __init__(self, credential, sl1_app, headers=None):
        self._credential = credential
        self._headers = headers
        self._sl1_app = sl1_app
        self.auth_token = None
        self._timeout = int(self._credential['cred_timeout']) /1000

    def get(self,uri):
        """
        Get configuration or performance information from Netscaler.
        Param:
            None
        Return:
            json
        """
        url = '{credurl}{uri}'.format(credurl=self._credential['curl_url'],uri=uri)
        auth = self.get_login()
        if auth is not None:
            headers={'cookie':auth}
            try:
                response = requests.get(url=url,timeout=self._timeout, verify=False)
                if response.status_code == requests.codes.get('ok'):
                    return response.json()
                else:
                    return None
            except ConnectTimeout as ct:
                self._sl1_app.debug(message='timeout occurred: {}'.format(ct))
            except ConnectionError as ce:
                self._sl1_app.debug(message='error: {}'.format(ce))

    def get_login(self):
        """
        Read cookie from cache or get it from the device
        Param:
            None
        Return:
            Auth cookie
        """
        return self._sl1_app.get_cache_data(comp_type='auth') or self.do_login()

    def do_login(self):
        """
        Do the actual login to NetScaler login.

        Param:
            None

        Return:
            Auth cookie
        """
        url = "{}/config/login".format(self._credential['curl_url'])
        body={ "login":{"username":self._credential['cred_user'],"password":self._credential['cred_pwd']} }
        try:
            response = requests.post(url=url,json=body, timeout=self._timeout, verify=False)
            if response.status_code == requests.codes.get('created'):
                cookie = NITRO_AUTH_TOKEN_KEY + '={}'.format(response.headers['Set-Cookie'].split('; ')[3].split('=')[2])
                self._sl1_app.debug(message='Successfully authenticated. Received cookie = "' + cookie + '"')
                self._sl1_app.cache_data(data=cookie, comp_type='auth' )
            elif response.status_code == requests.codes.get('unautorized'):
                self._sl1_app.debug(message='Could not authenticate with the provided credentials: {}'.format(response.text), ui_debug=True )
                cookie=None
            else:
                cookie=None
            return cookie
        except ConnectTimeout as ct:
            self._sl1_app.debug(message='timeout occurred: {}'.format(ct))
        except ConnectionError as ce:
            self._sl1_app.debug(message="error: {}".format(ce))

