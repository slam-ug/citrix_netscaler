from unittest import TestCase
from silo_netscaler.nitro_exceptions import NitroRequestException
from silo_netscaler.nitro_client import NitroClient
from mock import Mock
import responses

class TestNitroClient(TestCase):
    def setUp(self):
        self._uri ='https://127.0.0.1'
        self._sessionid = '0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789AB'
        self._goodcredential={'cred_user':'gooduser','cred_pwd':'goodpassword','curl_url':'{}/nitro/v1'.format(self._uri),'cred_timeout':5}
        self._badcredential={'cred_user':'baduser','cred_pwd':'badpassword','curl_url':'{}/nitro/v1'.format(self._uri),'cred_timeout':5}

    @responses.activate
    def test_successful_logon(self):
        responses.add(
            method='POST', 
            url='{}/config/login'.format(self._goodcredential['curl_url']), 
            status=201, 
            headers={'Set-Cookie':'SESSID=deleted; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0; path=/, sessionid=%23%23{}; path=/nitro/v1'.format(self._sessionid)}, 
            json={u'errorcode': 0, u'message': u'Done', u'sessionid': u'##{}'.format(self._sessionid), u'severity': u'NONE'}
        )
        expected = 'NITRO_AUTH_TOKEN=%23%23{}'.format(self._sessionid)
        sl1_app = Mock()
        sl1_app.get_cache_data = Mock(return_value=expected)
        client = NitroClient(credential=self._goodcredential, sl1_app=sl1_app )
        auth=client.get_login()
        self.assertEqual(expected,auth)

    @responses.activate
    def test_unsuccessful_logon(self):
        responses.add(
            method='POST', 
            url='{}/config/login'.format(self._badcredential['curl_url']), 
            status=401, 
            headers={'Set-Cookie':'SESSID=deleted; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0; path=/'}, 
            json={u'errorcode': 354, u'message': u'Invalid username or password', u'severity': u'ERROR'}
        )
        sl1_app = Mock()
        sl1_app.get_cache_data = Mock(return_value=None)
        client = NitroClient(credential=self._badcredential, sl1_app=sl1_app )
        auth=client.get_login()
        self.assertIsNone(auth)

    @responses.activate
    def test_get_with_cached_login_info(self):
        responses.add(
            method='GET',
            url='{}/config/nsconfig'.format(self._goodcredential['curl_url']),
            status=200,
            json={"errorcode":0,"message":"Done","severity":"NONE","nsconfig":{"ipaddress":"127.0.0.1","netmask":"255.255.255.0","tagged":"YES","maxconn":"0","maxreq":"0","cip":"DISABLED","cookieversion":"0","securecookie":"ENABLED","systemtype":"Stand-alone","primaryip":"127.0.0.1","pmtumin":"576","pmtutimeout":10,"flags":"4","timezone":"GMT+02:00-CEST-Europe/Brussels","lastconfigchangedtime":"Thu Apr  2 22:13:24 2020","lastconfigsavetime":"Tue Mar 24 00:42:38 2020","currentsytemtime":"Thu Apr  2 22:45:05 2020","systemtime":"1585860305","grantquotamaxclient":"10","exclusivequotamaxclient":"80","grantquotaspillover":"10","exclusivequotaspillover":"80","configchanged":True}}
        )
        expected=True
        sl1_app = Mock()
        sl1_app.get_cache_data = Mock(return_value='NITRO_AUTH_TOKEN=%23%23{}'.format(self._sessionid))
        client = NitroClient(credential=self._goodcredential, sl1_app=sl1_app)
        configresult = client.get(uri='/config/nsconfig')
        self.assertEqual(expected,'nsconfig' in configresult.keys())

    @responses.activate
    def test_get_with_do_login(self):
        responses.add(
            method='POST', 
            url='{}/config/login'.format(self._goodcredential['curl_url']), 
            status=201, 
            headers={'Set-Cookie':'SESSID=deleted; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0; path=/, sessionid=%23%23{}; path=/nitro/v1'.format(self._sessionid)}, 
            json={u'errorcode': 0, u'message': u'Done', u'sessionid': u'##{}'.format(self._sessionid), u'severity': u'NONE'}
        )
        responses.add(
            method='GET',
            url='{}/config/nsconfig'.format(self._goodcredential['curl_url']),
            status=200,
            json={"errorcode":0,"message":"Done","severity":"NONE","nsconfig":{"ipaddress":"127.0.0.1","netmask":"255.255.255.0","tagged":"YES","maxconn":"0","maxreq":"0","cip":"DISABLED","cookieversion":"0","securecookie":"ENABLED","systemtype":"Stand-alone","primaryip":"127.0.0.1","pmtumin":"576","pmtutimeout":10,"flags":"4","timezone":"GMT+02:00-CEST-Europe/Brussels","lastconfigchangedtime":"Thu Apr  2 22:13:24 2020","lastconfigsavetime":"Tue Mar 24 00:42:38 2020","currentsytemtime":"Thu Apr  2 22:45:05 2020","systemtime":"1585860305","grantquotamaxclient":"10","exclusivequotamaxclient":"80","grantquotaspillover":"10","exclusivequotaspillover":"80","configchanged":True}}
        )
        expected=True
        sl1_app = Mock()
        sl1_app.get_cache_data = Mock(return_value=None)
        client = NitroClient(credential=self._goodcredential, sl1_app=sl1_app)
        configresult = client.get(uri='/config/nsconfig')
        self.assertEqual(expected,'nsconfig' in configresult.keys())
