from silo_common.snippets.misc import generate_alert
from silo_common.global_definitions import APP_TYPE_BULK_SNIPPET_CONFIG, APP_TYPE_BULK_SNIPPET_PERF
from silo_netscaler.nitro_exceptions import NitroRequestException
import silo_common.snippets as em7_snippets

class NitroSL1App(object):
    def __init__(self, app, app_name, snippet_id, internal_alerts_by_device_id=None):
        self._app = app
        self._app_name = app_name
        self._app_type = app.app_type
        self._snippet_id = snippet_id
        self._logger = None
        self._cache = em7_snippets.cache_api(app)
        self.NITRO_INVENTORY_CACHE_KEY = 'NetScaler_Inventory_{}_{}'
        self.NITRO_DEFAULT_CACHE_TTL = 3600
        self.generate_alert = generate_alert
        if self._app_type in [APP_TYPE_BULK_SNIPPET_CONFIG, APP_TYPE_BULK_SNIPPET_PERF]:
            self._result_handler = em7_snippets.BulkObjectHandler(self._app.oids)
            self._result_handler._set_request(self._snippet_id)
        else: 
            self._result_handler = em7_snippets.ObjectHandler(self._app.oids)
            self._result_handler._set_request(self._snippet_id)
        if self._app is not None:
            self._logger = self._app.logger
        if internal_alerts_by_device_id is not None:
            self._internal_alerts = internal_alerts_by_device_id
        else:
            self._internal_alerts = app.internal_alerts

    def cache_data(self, data, comp_type, key=None, ttl=None):
        """
        Method to easily cache a nugget of information

        Param:

            data -> The Dictionary to be cached
            comp_type -> A custom identifier for key construction
        
        Return:

            None
        """
        if ttl is None:
            ttl = self.NITRO_DEFAULT_CACHE_TTL
        if not key:
            key = self.NITRO_INVENTORY_CACHE_KEY.format(self._app.root_did, comp_type)
        self._cache.cache_result(data, ttl=ttl, key=key, commit=True)

    def get_cache_data(self, comp_type, key=None):
        """
        Method to easily retrieve a cached nugget of information

        Param:

            data -> None
            comp_type -> A custom identifier for key construction

        Return:

            A Dictionary, containing data in dictionary format.
        """
        if not key and comp_type: 
            key = self.ARM_INVENTORY_CACHE_KEY.format(self._app.root_did, comp_type)
        return self._cache.get(key)

    def warn(self, message, did):
        """
        Method used to add a 3rd party message to the device log.

        Param:

            did -> Device Id
            message -> Message to send as a NetScaler warning alert
        
        Return:

            None
        """
        formatted_message = "{app_name} [{app_id}]: {message}".format(app_name=self.name,
                                                                      app_id=self.app_id,
                                                                      message=message)
        if self._logger is None:
            return None
        self.generate_alert(message=formatted_message, xid=did, xtype=1, yid=0,
                            ytype=12, yname='', value='', threshold='')

    def debug(self, message, ui_debug=False):
        """
        Method used to log the debug messages

        Param:

            ui_debug -> Send the debug to the UI
            message -> Message to send to debug

        Return:
        
            None
        """
        formatted_message = "{app_name} [{app_id}]: {message}".format(app_name=self.name,
                                                                      app_id=self.app_id,
                                                                      message=message)
        if self._logger is None:
            return None
        if ui_debug:
            self._logger.ui_debug(formatted_message)
        else:
            self._logger.debug(formatted_message)

    def object_ids(self, filter_ids=None):
        """
        Method used to get the oid values from the result handler
        :param filter_ids: filter the ids to use
        :return:
        """
        if self._app_type in [APP_TYPE_BULK_SNIPPET_CONFIG, APP_TYPE_BULK_SNIPPET_PERF]:
            oids = []
            use_filter = filter_ids and len(filter_ids) > 0
            for k, v in self.result_handler.items():
                if use_filter:
                    # if the device is in the filter list
                    if k[0] in filter_ids:
                        oids.append(v['oid'])
                else:
                    oids.append(v['oid'])
            return oids
        else:
            return self.result_handler.oids

    @property
    def device_id(self):
        if self._app is None:
            return 0
        return self._app.did

    @property
    def name(self):
        return self._app_name

    @property
    def snippet_id(self):
        return self._snippet_id

    @property
    def result_handler(self):
        return self._result_handler

    @property
    def root_device_id(self):
        if self._app is None:
            return 0
        return self._app.root_did

    @property
    def app_id(self):
        if self._app is None:
            return 0
        return self._app.app_id
