from unittest import TestCase
from mock import Mock
from collections import namedtuple
from silo_common.global_definitions import APP_TYPE_BULK_SNIPPET_CONFIG, APP_TYPE_BULK_SNIPPET_PERF

class SL1App(TestCase):
    def setUp(self):
        App = namedtuple('App','app_id app_type oids logger internal_alerts root_did did')
        self._bulk_config_dynapp_unmerged = App(1,APP_TYPE_BULK_SNIPPET_CONFIG, {},None,{},60,60)
        self._bulk_config_dynapp_merged = App(1,APP_TYPE_BULK_SNIPPET_CONFIG, {},None,{},20,60)
        self._bulk_perf_dynapp_unmerged = App(1,APP_TYPE_BULK_SNIPPET_PERF, {},None,{},60,60)
        self._bulk_perf_dynapp_merged = App(1,APP_TYPE_BULK_SNIPPET_PERF, {},None,{},20,60)
        self._snippet_id = 485
        self._app_name = 'mytestdynapp'
    def test_sl1_bulk_config_unmerged_app(self):
        from silo_netscaler.nitro_sl1_app import NitroSL1App
        app = NitroSL1App(self._bulk_config_dynapp_unmerged,self._app_name, self._snippet_id)
        self.assertIsNotNone(app)
    def test_sl1_bulk_perf_unmerged_app(self):
        from silo_netscaler.nitro_sl1_app import NitroSL1App
        app = NitroSL1App(self._bulk_perf_dynapp_unmerged,self._app_name, self._snippet_id)
        self.assertIsNotNone(app)
    def test_sl1_bulk_config_merged_app(self):
        from silo_netscaler.nitro_sl1_app import NitroSL1App
        app = NitroSL1App(self._bulk_config_dynapp_merged,self._app_name, self._snippet_id)
        self.assertIsNotNone(app)
    def test_sl1_bulk_perf_merged_app(self):
        from silo_netscaler.nitro_sl1_app import NitroSL1App
        app = NitroSL1App(self._bulk_perf_dynapp_merged,self._app_name, self._snippet_id)
        self.assertIsNotNone(app)
