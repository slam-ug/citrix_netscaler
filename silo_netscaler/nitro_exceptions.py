class NitroRequestException(Exception):
    def __init__(self, message, error_code=0, alert_message=None):
        super(NitroRequestException, self).__init__(message)
        self._alert_message = alert_message
        self._error_code = error_code

        if self._error_code > 0:
            message += " Code: {number}".format(number=self._error_code)

        if self._alert_message is None:
            self._alert_message = message

    @property
    def alert_message(self):
        return self._alert_message

    @property
    def error_code(self):
        return self._error_code