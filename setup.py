import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="silo_netscaler", # Replace with your own username
    version="0.0.1",
    author="Tom Robijns",
    author_email="trammeke@hotmail.com",
    description="ScienceLogic NetScaler package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/slam-ug/citrix_netscaler",
    packages=setuptools.find_packages(exclude=["tests"]),
    classifiers=[
        "Programming Language :: Python :: 2",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=2.7',
)