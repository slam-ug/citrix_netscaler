# Citrix NetScaler NITRO

## Summary

See [Citrix ADC API Reference](https://developer-docs.citrix.com/projects/citrix-adc-nitro-api-reference/en/latest/).

## API Versioning

The API acts differently depending on the version of the ADC (Application Delivery Controller). See [Changes accross releases](https://developer-docs.citrix.com/projects/citrix-adc-nitro-api-reference/en/latest/nitro-changes-across-releases/) for more information.

## structure

We create a nitro_client that will do the calls to the Citrix NetScaler.

The sl1_app mimicks all necessary options that a dynamic application provides. We use this to be able to call & test (by Mocking) our modules without having to call them via a dynapp.

nitro_exceptions provides a basic exception we will use throughout this silo_netscaler package.

The nitro_resources package provides a list of constants that can be used to make the code more readable.
