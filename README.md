[![pipeline status](https://gitlab.com/slam-ug/citrix_netscaler/badges/master/pipeline.svg)](https://gitlab.com/slam-ug/citrix_netscaler/-/commits/master) [![coverage report](https://gitlab.com/slam-ug/citrix_netscaler/badges/master/coverage.svg)](https://gitlab.com/slam-ug/citrix_netscaler/-/commits/master)

# citrix ADC Netscaler

PowerPack for Citrix ADC NetScaler. All documentation can be found in [docs](docs/netscaler-nitro.md)